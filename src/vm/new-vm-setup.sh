#!/bin/bash

skip_dependency_check_parameter=--skip-dependency-check
skip_dependency_check_parameter_short=-s
skip_dependency_check=false

SHORT=${skip_dependency_check_parameter_short:1} # : after flag name indicates that value with argument is required
LONG=${skip_dependency_check_parameter:2} # : after flag name indicates that value with argument is required

OPTS=$(getopt --options $SHORT --long $LONG --name "$0" -- "$@")
if [ $? != 0 ] ; then echo "Failed to parse arguments, exiting" >&2 ; exit 1 ; fi
eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    $skip_dependency_check_parameter | $skip_dependency_check_parameter_short )
      skip_dependency_check=true
      shift
      ;;
    -- )
      shift
      break
      ;;
    *)
      echo "Error while parsing arguments"
      exit 1
      ;;
  esac
done

function check_dependencies () {
	echo "Checking if we are on supported OS version"

	os_id=$(grep "^ID=" /etc/os-release | cut -d'=' -f2 | tr -d '"')
	os_version=$(grep "^VERSION_ID=" /etc/os-release | cut -d'=' -f2 | tr -d '"')

	if [ "$os_id" == "debian" ] && [ "$os_version" == "11" ]; then
		echo "Os ${os_id} with version ${os_version} is supported"
	else
		echo "Os ${os_id} with version ${os_version} is NOT supported"
		exit 1
	fi

	sudo apt-get update
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi

	sudo apt install git
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
	git --version
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi

	sudo apt-get install ca-certificates curl gnupg lsb-release
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
	sudo mkdir -p /etc/apt/keyrings
	curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi

	sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
	docker --version
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
	docker-compose --version
	if [ $? -ne 0 ]; then echo "Dependency check failure"; exit 1; fi
}

function create_user() {
	if id "$1" &>/dev/null; then
		echo "User $1 already exist"
	else
		sudo useradd -m -d /home/$1 -s /bin/bash $1
		sudo passwd $1
	fi
}

# ---

if [ "$skip_dependency_check" == "false" ]; then
	check_dependencies
else
	echo "Checking dependencies skipped"
fi

echo "Setting up user"
read -p 'User name: ' user_name
create_user $user_name

if [ ! -d "/home/$user_name/app" ]; then
	sudo su - $user_name -c "git clone https://gitlab.com/tometchy/softwaredeveloperserver.git ~/app; mkdir -p ~/app/src/sitepublish/.private-settings; mkdir -p ~/app/src/crm/.private-settings"
fi

read -s -p 'Sendgrid api key: ' sendgrid_key

echo "Saving env parameters for Docker Compose"
sudo su - $user_name -c "echo \"APP_USER=$user_name\" > ~/app/src/.env; echo \"APP_USER_ID=$(id -u $user_name)\" >> ~/app/src/.env; echo \"SENDGRID_KEY=$sendgrid_key\" >> ~/app/src/.env"

echo "Install snapd which is required by certbot"
sudo apt-get install snapd
sudo snap install core; sudo snap refresh core
sudo snap install hello-world
hello-world

echo "Install certbot"
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
echo "Stop webserver and run: sudo certbot certonly --standalone"

echo "Creating cron logs directories"
current_user_id=$(id -u)
current_user_name=$(id -un)
sudo mkdir -pv /var/log/cron/$current_user_name
sudo chown $current_user_id /var/log/cron/$current_user_name

sudo mkdir -pv /var/log/cron/$user_name
sudo chown $user_name /var/log/cron/$user_name
 
sudo crontab -l -u $user_name > tmp.txt
 if grep -q -wi "update-server-repo.sh" tmp.txt; then
 	echo "Update server repo script already set as a crone job"
 else
 	echo "Setting update server repo script as a cron job"
	current_date_text='$(date +\%Y\%m\%d\%H\%M\%S)'
 	sudo crontab -l -u $user_name | { cat; echo "* * * * * cd /home/$user_name/app/src/vm/ && /bin/bash update-server-repo.sh  >> /var/log/cron/$user_name/update-server-repo_$current_date_text.log 2>&1"; } | sudo crontab -u $user_name -
 	echo "Cron job added"
 	sudo crontab -l -u $user_name
 fi 
rm -f tmp.txt

crontab -l > tmp.txt
 if grep -q -wi "restart-app-if-update-available.sh" tmp.txt; then
 	echo "Restart app script already set as a crone job"
 else
 	echo "Setting restart app script as a cron job"
	sudo touch /home/$user_name/app-version/current
	sudo chown $current_user /home/$user_name/app-version/current
	current_date_text='$(date +\%Y\%m\%d\%H\%M\%S)'
 	crontab -l | { cat; echo "* * * * * /bin/bash /home/$user_name/app/src/vm/restart-app-if-update-available.sh $user_name >> /var/log/cron/$current_user_name/restart_app_if_update_available_$current_date_text.log 2>&1"; } | crontab -
 	echo "Cron job added"
 	crontab -l
 fi 
rm -f tmp.txt