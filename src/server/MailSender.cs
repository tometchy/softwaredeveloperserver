using System.Net;
using System.Text;
using SendGrid;
using SendGrid.Helpers.Mail;
using server.Request;

namespace server;

public class MailSender : IMailSender
{
    private static string _sendgridKey = Environment.GetEnvironmentVariable("SENDGRID_KEY") ?? throw new Exception("SENDGRID_KEY not declared");
    private const string FROM_NAME = "Tom Git";
    private const string FROM_MAIL = "tom@devops.broker";
    private const string SYSTEM_EMAIL = "system@devops.broker";
    private readonly ILogger<MailSender> _logger;

    public MailSender(ILogger<MailSender> logger) => _logger = logger;

    public async Task<MailStatus> SendAsync(string? toEmail, string? toName, string title, string body, string? from = null)
    {
        // TBD - https://mailtrap.io/blog/list-unsubscribe-header/
        var message = new SendGridMessage();

        message.Personalizations = new List<Personalization>() {
            new Personalization(){
                Tos = new List<EmailAddress>(){
                    new EmailAddress(){
                        Email = toEmail,
                        Name = toName
                    }
                },
                Bccs = new List<EmailAddress>(){
                    new EmailAddress(){
                        Email = "tometchy@tutanota.com",
                        Name = "tometchy@tutanota.com"
                    }
                }
            }
        };

        message.From = new EmailAddress()
        {
            Email = from ?? FROM_MAIL,
            Name = from ?? FROM_NAME
        };

        message.Subject = title;

        message.Contents = new List<Content>(){
            new Content(){
                Type = "text/html",
                Value = body
            }
        };

        message.TrackingSettings = new TrackingSettings()
        {
            ClickTracking = new ClickTracking()
            {
                Enable = false,
                EnableText = false
            },
            OpenTracking = new OpenTracking()
            {
                Enable = true,
                SubstitutionTag = "%open-track%"
            },
            SubscriptionTracking = new SubscriptionTracking()
            {
                Enable = false
            }
        };

        var client = new SendGridClient(_sendgridKey);
        var response = await client.SendEmailAsync(message).ConfigureAwait(false);
        var responseBody = await response.Body.ReadAsStringAsync();

        Console.WriteLine(response.StatusCode.ToString());
        _logger.LogInformation(response.StatusCode.ToString());

        Console.WriteLine(responseBody);
        _logger.LogInformation(responseBody);

        Console.WriteLine(response.Headers.ToString());
        _logger.LogInformation(response.Headers.ToString());

        if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK)
            return Success.Instance;
        return Failure.Instance;
    }

    public void InformUs(HttpContext context, DecodedRequest request)
    {
        var headersBuilder = new StringBuilder(Environment.NewLine);
        foreach (var header in context.Request.Headers)
            headersBuilder.AppendLine($"{header.Key}: {header.Value}");
        var headersDump = headersBuilder.ToString();

        var informUsMessage = $@"
        REQUEST: {request.ToString()}<br><br>

        {headersDump}<br><br>

        CLIENT IP and PORT:<br>
        {context.Connection.RemoteIpAddress}<br>
        {context.Connection.RemotePort}<br><br>

        SERVER IP and PORT:<br>
        {context.Connection.LocalIpAddress}<br>
        {context.Connection.LocalPort}<br><br>
        ";

        SendAsync("tometchy@tutanota.com", "tometchy@tutanota.com", $"{request.Email} {request.RequestType} INFORM US", informUsMessage, SYSTEM_EMAIL);
    }
}