if [ "$1" == "--show" ]; then
	echo "Runnig in foreground"
else
	echo "Runnig in background"
	additional_parameters=" -d"
fi

sudo docker-compose up --build --no-recreate${additional_parameters}
