#!/bin/bash

if [ "$(git status &>/dev/null ; echo $?)" -ne "0" ]; then
    echo "You must run this script inside repo, current working directory: $(pwd)"
    exit 1
fi

if [ "$(stat -c '%U' .)" != "$(id -u -n)" ]; then 
    echo "You must run this script as repo owner user: $(stat -c '%U' $repo_path), currently is: $(id -u -n)"
    exit 1
fi;

git fetch
git status
git reset --hard origin/master
git status

last_commit_id=$(git log --pretty=format:%H -1)
echo "Last commit id: ${last_commit_id}"

mkdir -p ~/app-version
echo $last_commit_id > ~/app-version/the-newest