namespace server.Mail;

public static class MailFactory
{
    public static string CreateBody(string body)
    {
        return @$"
<!DOCTYPE html><html>
  <head>

    <meta http-equiv=""content-type"" content=""text/html; charset="">
  </head>
  <body>
    {body}
  </body>
</html>
";
    }
}