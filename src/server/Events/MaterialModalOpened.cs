using server.Request;

namespace server.Events;

public class MaterialModalOpened : HttpRequestReceived
{
    private readonly DecodedRequest _request;

    public MaterialModalOpened(HttpContext context, DecodedRequest request)
        : base(context) => _request = request;

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(MaterialModalOpened)}_{_request.MaterialName}.md"), ToString());
    }

    public override string ToString() => $"[{nameof(MaterialModalOpened)} >> {base.ToString()}; {_request.ToString()}]";
}