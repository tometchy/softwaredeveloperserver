namespace server.Mail;

public class RegularMail : IMail
{
    public RegularMail(string title, string content)
    {
        Title = title;
        Content = content;
    }

    public string Title { get; }
    public string Content { get; }
}