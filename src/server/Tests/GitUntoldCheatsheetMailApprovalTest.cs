using NUnit.Framework;
using server.Mail;

namespace server.Tests;

public class GitUntoldCheatsheetMailApprovalTest
{
    private const string FlName = "Alice";
    private const string Email = "alice@example.com";

    [Test]
    public void Test()
    {
        var response = new MailCreator().CreateUntoldCheatsheetMail(new FakeRequest { Flname = FlName, Email = Email});
        Assert.AreEqual("Download Git Untold Interface Cheat Sheet", response.Title);
        Assert.AreEqual(@"<div>
            Hi Alice,
                <div><br>
                    here you can <a href=""https://devops.broker/7e0f8"">download <i>Git Untold Interface Cheat Sheet</i></a>.
                </div>
                
<div>
    Want to improve further?
    <br><br>
    Your company can pay for the training, I will arrange everything, just let me know where you work.
    <br><br>
    <b>Training topics</b>
    <ul>
        <li>What are those abstract <strong><i>BRANCHES</i></strong> thing?</li>
        <li>What is <strong><i>HEAD</i></strong> and do we need to understand this?</li>
        <li>What to do when we have commited or pushed something wrong?
            <strong><i>RESET</i></strong>, <strong><i>REVERT</i></strong>,
            <strong><i>REBASE</i></strong> or
            maybe <strong><i>COMMIT --AMMEND</i></strong>?
        </li>
        <li>Should we <strong><i>REBASE</i></strong> or <strong><i>MERGE</i></strong>? 
            When and why sometimes rebasing is painful?
            What to do <strong>to always have its advantages</strong> yet avoid its weak points?
        </li>
        <li>How and when to use <strong><i>CHERRY-PICK</i></strong>?</li>
        <li>Can we <strong><i>PUSH FORCE WITH LEASE</i></strong>, and <strong><i>PULL WITH REBASE</i></strong> instead merge?</li>
        <li>What are <strong>good commit message</strong> rules and why we should follow
            them?</li>
        <li>How to <strong>optimize work environment</strong>? What are <strong>Git configuration
                rules</strong>? 
        </li>
        <li>
            How to use <strong><i>.gitattributes</i> file</strong> and how does it differ from <strong><i>.gitignore</i> file</strong>?
        </li>
    </ul>
</div>

                
    <div>
        <br>
        <i>Give my regards to the team,<br>
        Tom Skraskowski<br><a href=""https://www.devops.broker"">DevOps.Broker</a></i><br>
        <div>
        <img alt=""Tom"" height=""90""
            src=""https://devops.broker/images/email-avatar.png?i=alice@example.com""
            style=""border:0px;width:90px;height:90px;margin:10px;outline:none;text-decoration:none""
            width=""90"">
        </div>

            <div>
      <table width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0""
        bgcolor=""#ffffff"" align=""left"">
        <tbody>
          <tr>
            <td>
              <table style=""width:640px;min-width:640px"" width=""640""
                cellspacing=""0"" cellpadding=""0"" border=""0"" bgcolor=""#ffffff"" align=""center"">
                <tbody>
                  <tr>
                    <td>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td height=""10""><br>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td style=""padding:0px 40px"" align=""center"">
                              <table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                                <tbody>
                                  <tr>
                                    <td align=""center"">
                                      <table style=""width:267px;min-width:267px""                                      
                                        width=""267"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""left"">
                                        <tbody>
                                          <tr>
                                            <td id=""m_-3128637692470407947footerText-20""
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""left"">
                                              <p style=""margin-top:0px;margin-bottom:0px"">Tomasz Skraskowski
                                                DevOps.Broker<br>
                                                Licealna 34, 87-100<br>
                                                Toruń, Poland</p>
                                              <p style=""margin-top:0px;margin-bottom:0px""><a
                                                  href=""https://devops.broker/privacy-policy.pdf"">Privacy Policy</a><br>
                                              </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table style=""width:267px;min-width:267px""
                                         width=""267"" cellspacing=""0""
                                        cellpadding=""0"" border=""0"" align=""right"">
                                        <tbody>
                                          <tr>
                                            <td id=""m_-3128637692470407947footerUnsubscribeText-20""
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""right"">
                                              <p style=""margin-top:0px;margin-bottom:0px"">You received this email
                                                because you signed up on our website. You can always change your mind
                                                with button below.</p>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td height=""10""><br>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""right""> <a href=""https://devops.broker/optout?email=alice@example.com""
                                                style=""color:grey;text-decoration:underline"" target=""_blank"">
                                                <span style=""color:grey"">Unsubscribe</span> </a> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td style=""line-height:40px;min-height:40px""
                              height=""40""><br>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
        
            </div>", response.Content);
    }
}