using server.Request;

namespace server.Tests;

public class FakeRequest : IRequest
{
    public string? Email { get; set; }
    public string? Phone { get; set; }
    public string? Flname { get; set; }
    public string? Company { get; set; }
    public string? Message { get; set; }
    public string? Url { get; set; }
    public string? UrlSearch { get; set; }
    public string? UserAgent { get; set; }
    public string? Time { get; set; }
    public string? RequestType { get; set; }
    public string? MaterialName { get; set; }
}