using server.Request;

namespace server.Mail;

public class MailCreator
{
    public static readonly string CommonMaterialsPartDiv = @"
<div>
    Want to improve further?
    <br><br>
    Your company can pay for the training, I will arrange everything, just let me know where you work.
    <br><br>
    <b>Training topics</b>
    <ul>
        <li>What are those abstract <strong><i>BRANCHES</i></strong> thing?</li>
        <li>What is <strong><i>HEAD</i></strong> and do we need to understand this?</li>
        <li>What to do when we have commited or pushed something wrong?
            <strong><i>RESET</i></strong>, <strong><i>REVERT</i></strong>,
            <strong><i>REBASE</i></strong> or
            maybe <strong><i>COMMIT --AMMEND</i></strong>?
        </li>
        <li>Should we <strong><i>REBASE</i></strong> or <strong><i>MERGE</i></strong>? 
            When and why sometimes rebasing is painful?
            What to do <strong>to always have its advantages</strong> yet avoid its weak points?
        </li>
        <li>How and when to use <strong><i>CHERRY-PICK</i></strong>?</li>
        <li>Can we <strong><i>PUSH FORCE WITH LEASE</i></strong>, and <strong><i>PULL WITH REBASE</i></strong> instead merge?</li>
        <li>What are <strong>good commit message</strong> rules and why we should follow
            them?</li>
        <li>How to <strong>optimize work environment</strong>? What are <strong>Git configuration
                rules</strong>? 
        </li>
        <li>
            How to use <strong><i>.gitattributes</i> file</strong> and how does it differ from <strong><i>.gitignore</i> file</strong>?
        </li>
    </ul>
</div>
";
    
    public IMail CreateContactRequestConfirmationMail(IRequest request) => new RegularMail(
        "I have received your message",
        $@"
<div>
        Hi {request.Flname},
            <div><br>
                I have just received your message and will reply to you as soon as possible.
            </div>
            <div><br>
                In the meanwhile check out our <a href=""https://devops.broker/free-materials?i=m"">downloadable free materials</a>, which you can start using right away.
                <br>If you find them valuable, share them with others in your organization.
            </div>
            <div><br>
                After all, <b>you make a strong team together</b> 💪
                <br><br><i>Give my regards to the team,<br>
                Tom Skraskowski<br><a href=""https://www.devops.broker"">DevOps.Broker</a></i><br>
            </div>
        </div>
"
    );

    public IMail CreateUntoldCheatsheetMail(IRequest request) => CreateMail(request,
        "Download Git Untold Interface Cheat Sheet",
        $@"here you can <a href=""https://devops.broker/{Materials.GitUntoldId}"">download <i>Git Untold Interface Cheat Sheet</i></a>.");

    public IMail CreateLogCheatsheetMail(IRequest request) => CreateMail(request,
        "Download Advance Git Log Cheat Sheet",
        $@"here you can <a href=""https://devops.broker/{Materials.GitLogId}"">download <i>Advance Git Log Cheat Sheet</i></a>.");

    public IMail CreateTricksMail(IRequest request) => CreateMail(request,
        "Download 11 Git Tricks To Boost Your Productivity",
        $@"here you can <a href=""https://devops.broker/{Materials.GitTricksId}"">download <i>11 Git Tricks To Boost Your Productivity</i></a>.");

    public IMail CreateIntroductionCheatsheetMail(IRequest request) => CreateMail(request,
        "Download Git Introduction Cheat Sheet",
        $@"here you can <a href=""https://devops.broker/{Materials.GitIntroductionId}"">download <i>Git Introduction Cheat Sheet</i></a>.");
    
    private IMail CreateMail(IRequest request, string title, string uniqueLine) => new RegularMail(title,
        $@"<div>
            Hi {request.Flname},
                <div><br>
                    {uniqueLine}
                </div>
                {CommonMaterialsPartDiv}
                {FooterDiv.Create(request.Email)}
            </div>"
    );
}