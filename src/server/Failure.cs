






namespace server;

public class Failure : MailStatus {
    public static MailStatus Instance { get; } = new Failure();
    private Failure() {}
}