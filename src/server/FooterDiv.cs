namespace server;

public static class FooterDiv
{
  public static string Create(string email) =>
    @$"
    <div>
        <br>
        <i>Give my regards to the team,<br>
        Tom Skraskowski<br><a href=""https://www.devops.broker"">DevOps.Broker</a></i><br>
        <div>
        <img alt=""Tom"" height=""90""
            src=""https://devops.broker/images/email-avatar.png?i={email}""
            style=""border:0px;width:90px;height:90px;margin:10px;outline:none;text-decoration:none""
            width=""90"">
        </div>

            <div>
      <table width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0""
        bgcolor=""#ffffff"" align=""left"">
        <tbody>
          <tr>
            <td>
              <table style=""width:640px;min-width:640px"" width=""640""
                cellspacing=""0"" cellpadding=""0"" border=""0"" bgcolor=""#ffffff"" align=""center"">
                <tbody>
                  <tr>
                    <td>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td height=""10""><br>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td style=""padding:0px 40px"" align=""center"">
                              <table width=""100%"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                                <tbody>
                                  <tr>
                                    <td align=""center"">
                                      <table style=""width:267px;min-width:267px""                                      
                                        width=""267"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""left"">
                                        <tbody>
                                          <tr>
                                            <td id=""m_-3128637692470407947footerText-20""
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""left"">
                                              <p style=""margin-top:0px;margin-bottom:0px"">Tomasz Skraskowski
                                                DevOps.Broker<br>
                                                Licealna 34, 87-100<br>
                                                Toruń, Poland</p>
                                              <p style=""margin-top:0px;margin-bottom:0px""><a
                                                  href=""https://devops.broker/privacy-policy.pdf"">Privacy Policy</a><br>
                                              </p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table style=""width:267px;min-width:267px""
                                         width=""267"" cellspacing=""0""
                                        cellpadding=""0"" border=""0"" align=""right"">
                                        <tbody>
                                          <tr>
                                            <td id=""m_-3128637692470407947footerUnsubscribeText-20""
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""right"">
                                              <p style=""margin-top:0px;margin-bottom:0px"">You received this email
                                                because you signed up on our website. You can always change your mind
                                                with button below.</p>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td height=""10""><br>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              style=""font-family:'Poppins',sans-serif;font-size:12px;line-height:18px;color:grey""
                                              align=""right""> <a href=""https://devops.broker/optout?email={email}""
                                                style=""color:grey;text-decoration:underline"" target=""_blank"">
                                                <span style=""color:grey"">Unsubscribe</span> </a> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table style=""width:640px;min-width:640px""
                        width=""640"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">
                        <tbody>
                          <tr>
                            <td style=""line-height:40px;min-height:40px""
                              height=""40""><br>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
        ";
}