using Microsoft.Extensions.Primitives;

namespace server;

public static class StringExtensions
{
    public static bool HasValue(this StringValues text)
    {
        if(string.IsNullOrWhiteSpace(text))
            return false;

        return text != StringValues.Empty;
    }
}