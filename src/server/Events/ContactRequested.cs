using server.Request;

namespace server.Events;

public class ContactRequested : HttpRequestReceived
{
    private readonly DecodedRequest _request;

    public ContactRequested(HttpContext context, DecodedRequest request)
        : base(context) => _request = request;

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(ContactRequested)}.md"), ToString());
    }

    public override string ToString() => $"[{nameof(ContactRequested)} >> {base.ToString()}; {_request}]";
}