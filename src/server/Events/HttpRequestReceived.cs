using System.Text;

namespace server.Events;

public abstract class HttpRequestReceived : Event
{
    public HttpRequestReceived(HttpContext context, string? clientId = null)
    {
        Timestamp = DateTime.UtcNow;
        ClientIp = context.Connection.RemoteIpAddress.ToString();
        ClientPort = context.Connection.RemotePort.ToString();
        ServerIp = context.Connection.LocalIpAddress.ToString();
        ServerPort = context.Connection.LocalPort.ToString();

        RequestPath = context.Request.Path.ToString() + context.Request.QueryString.ToString();
        Referer = string.Empty;
        if (context.Request.Headers[CommonNames.Referer].HasValue())
            Referer = context.Request.Headers[CommonNames.Referer];

        var headersBuilder = new StringBuilder(Environment.NewLine);
        foreach (var header in context.Request.Headers)
            headersBuilder.Append($"[{header.Key}: {header.Value}]");
        Headers = headersBuilder.ToString();

        ClientId = clientId ?? context.Request.Cookies[CommonNames.ClientId];
        if (string.IsNullOrWhiteSpace(ClientId))
            ClientId = "MISSING";

        QueryId = context.Request.Cookies[CommonNames.QueryId] ?? string.Empty;
    }

    public string ClientIp { get; }
    public string ClientPort { get; }
    public string ServerIp { get; }
    public string ServerPort { get; }
    public string Headers { get; }
    public override DateTime Timestamp { get; }
    public string Referer { get; }
    public string RequestPath { get; }
    public string RequestPathParsed => RequestPath.Replace('/', '+');
    public string QueryId { get; }
    public override string ClientId { get; }
    public override string ClientIdShort => ClientId.Substring(0, 5);

    protected override void PersistEventToHdd(Context context)
    {
        if(!string.IsNullOrWhiteSpace(QueryId))
        {
            var queryIdDataDirectory = Path.Combine(context.DataDirectory, "query-id");
            Directory.CreateDirectory(queryIdDataDirectory);
            File.WriteAllText(Path.Combine(queryIdDataDirectory, QueryId), string.Empty);
        }
    }

    public override string ToString() => $"[{nameof(HttpRequestReceived)} >> " +
                                         $"Timestamp : {Timestamp}; " +
                                         $"Client id short: {ClientIdShort}; " +
                                         $"Referer: {Referer}; " +
                                         $"Request path: {RequestPath}; " +
                                         $"Client ip: {ClientIp}; " +
                                         $"Client port: {ClientPort}; " +
                                         $"Server ip: {ServerIp}; " +
                                         $"Server port: {ServerPort}; " +
                                         $"Query id: {QueryId}; " +
                                         $"Client id: {ClientId}; " +
                                         $"Headers: {Headers} ]";
}