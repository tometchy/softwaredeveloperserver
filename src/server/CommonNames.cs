namespace server;

public static class CommonNames
{
    public const string ClientId = "ClientId";
    public const string QueryId = "QueryId";
    public const string Referer = "Referer";
}