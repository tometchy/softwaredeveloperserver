using System.Text;

namespace server.Request;

public class DecodedRequest : IRequest
{
    public string? Email { get; }

    public string? Phone { get; }

    public string? Flname { get; }

    public string? Company { get; }

    public string? Message { get; }

    public string? Url { get; }

    public string? UrlSearch { get; }

    public string? UserAgent { get; }

    public string? Time { get; }

    public string? RequestType { get; }

    public string? MaterialName { get; }
    
    public DecodedRequest(Base64EncodedRequest encodedRequest)
    {
        Email = DecodeBase64(encodedRequest.Email);
        Phone = DecodeBase64(encodedRequest.Phone);
        Flname = DecodeBase64(encodedRequest.Flname);
        Company = DecodeBase64(encodedRequest.Company);
        Message = DecodeBase64(encodedRequest.Message);
        Url = DecodeBase64(encodedRequest.Url);
        UrlSearch = DecodeBase64(encodedRequest.UrlSearch);
        UserAgent = DecodeBase64(encodedRequest.UserAgent);
        Time = DecodeBase64(encodedRequest.Time);
        RequestType = DecodeBase64(encodedRequest.RequestType);
        MaterialName = DecodeBase64(encodedRequest.MaterialName);
    }

    private string DecodeBase64(string? encoded)
    {
        if (string.IsNullOrEmpty(encoded))
            return string.Empty;

        byte[] bytes = Convert.FromBase64String(encoded);
        return Encoding.Unicode.GetString(bytes);
    }

    public override string ToString() => $"[{nameof(DecodedRequest)} >> Email: {Email}; " +
                                         $"Phone: {Phone}; " +
                                         $"First and last name: {Flname}; " +
                                         $"Company: {Company}; " +
                                         $"Message: {Message}; " +
                                         $"Url: {Url}; " +
                                         $"Url search: {UrlSearch}; " +
                                         $"Time: {Time}; " +
                                         $"Request type: {RequestType}; " +
                                         $"Material name: {MaterialName}; " +
                                         $"User agent: {UserAgent}]";
}