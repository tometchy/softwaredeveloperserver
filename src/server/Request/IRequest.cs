namespace server.Request;

public interface IRequest
{
    public string? Email { get; }

    public string? Phone { get; }

    public string? Flname { get; }

    public string? Company { get; }

    public string? Message { get; }

    public string? Url { get; }

    public string? UrlSearch { get; }

    public string? UserAgent { get; }

    public string? Time { get; }

    public string? RequestType { get; }

    public string? MaterialName { get; }
}