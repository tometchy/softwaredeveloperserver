namespace server;

public abstract class Event
{
    public abstract DateTime Timestamp { get; }
    public abstract string ClientId { get; }
    public abstract string ClientIdShort { get; }

    public void SaveToHdd()
    {
        var clientDirectory = $"/crm-repo/www/{ClientIdShort}";
        var clientActionsDirectory = Path.Combine(clientDirectory, "actions");
        var clientDataDirectory = Path.Combine(clientDirectory, "data");
        var clientDataIdDirectory = Path.Combine(clientDataDirectory, "id");

        Directory.CreateDirectory(clientDirectory);
        Directory.CreateDirectory(clientActionsDirectory);
        Directory.CreateDirectory(clientDataDirectory);
        Directory.CreateDirectory(clientDataIdDirectory);

        File.WriteAllText(Path.Combine(clientDataIdDirectory, ClientId), string.Empty);

        PersistEventToHdd(new Context
        {
            ActionsDirectory = clientActionsDirectory,
            DataDirectory = clientDataDirectory
        });
    }

    protected abstract void PersistEventToHdd(Context context);

    protected class Context
    {
        public string ActionsDirectory { get; init; }
        public string DataDirectory { get; init; }
    }
}