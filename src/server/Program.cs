using server;
using server.Mail;

var builder = WebApplication.CreateBuilder(new WebApplicationOptions { Args = args, });

if (bool.TryParse(Environment.GetEnvironmentVariable("DEBUG"), out var isDebug) && isDebug)
{
    builder.Services.AddSingleton<IMailSender, FakeMailSender>();
    Console.WriteLine("Mocking third party services");
}
else
    builder.Services.AddSingleton<IMailSender, MailSender>();

builder.Services.AddSingleton<MailCreator>();
builder.Services.AddSingleton<StatsSaver>();

var app = builder.Build();

app.UseHsts();
app.UseHttpsRedirection();

app.Use(async (context, next) =>
{
    if (context.Request.Path.ToString().Length > 1 && context.Request.Path.ToString().EndsWith('/'))
        context.Response.Redirect(context.Request.Path.ToString().TrimEnd('/'));
    else
        await next.Invoke();
});

var statsSaver = app.Services.GetService<StatsSaver>();
statsSaver.Start();
app.Use(async (context, next) =>
{
    try
    {
        if (string.Equals(context.Request.Headers["sec-fetch-dest"], "document", StringComparison.InvariantCultureIgnoreCase) ||
            context.Request.Path.ToString().ToLower().Contains("email-avatar.png"))
        {
            var clientId = context.Request.Cookies[CommonNames.ClientId];
            if (string.IsNullOrWhiteSpace(clientId))
                clientId = Guid.NewGuid().ToString();
            context.Response.Cookies.Append(CommonNames.ClientId, clientId);

            var queryId = context.Request.Cookies[CommonNames.QueryId] ?? string.Empty;
            if (!string.IsNullOrWhiteSpace(context.Request.Query["i"].ToString()))
                queryId = context.Request.Query["i"].ToString();
            if (!string.IsNullOrWhiteSpace(queryId))
                context.Response.Cookies.Append(CommonNames.QueryId, queryId);

            if(context.Request.Path.ToString().ToLower().Contains("email-avatar.png"))
                statsSaver.OnEmailOpened(context, clientId, queryId);
            else
                statsSaver.OnWebsiteVisited(context, clientId);
        }
    }
    catch (Exception exeption)
    {
        Console.WriteLine($"Exception while processing request: {exeption}");
    }
    await next.Invoke();
});

app.UseStatusCodePages(async context =>
{
    context.HttpContext.Response.ContentType = System.Net.Mime.MediaTypeNames.Text.Html;

    var pathEnd = context.HttpContext.Request.Path.ToString().Trim('/') + ".html";
    if (context.HttpContext.Response.StatusCode == 404 && File.Exists(CreatePath()))
        context.HttpContext.Response.StatusCode = 200;
    else
        pathEnd = context.HttpContext.Response.StatusCode == 404 ? "404.html" : "error.html";

    await context.HttpContext.Response.WriteAsync(File.ReadAllText(CreatePath()));

    string CreatePath() => Path.Combine(builder.Environment.ContentRootPath, "wwwroot", pathEnd);
});

app.UseDefaultFiles();
app.UseStaticFiles();
app.UseControllers(builder, statsSaver);

app.Run();