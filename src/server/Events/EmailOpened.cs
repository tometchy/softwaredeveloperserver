namespace server.Events;

public class EmailOpened : HttpRequestReceived
{
    private readonly string _emailId;
    private readonly string _originalClientId; // Probably gmail server here

    public EmailOpened(HttpContext context, string clientId, string emailId)
        : base(context, emailId)
    {
        _emailId = emailId;
        _originalClientId = clientId;
    }

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(EmailOpened)}_{_emailId}.md"), ToString());
    }

    public override string ToString() => $"[{nameof(EmailOpened)} >> Email id: {_emailId}; Original client id: {_originalClientId}; {base.ToString()}]";
}