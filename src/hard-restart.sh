if [ "$1" == "--show" ]; then
	echo "Will run in foreground"
else
	echo "Will run in background"
	additional_parameters=" -d"
fi

sudo docker-compose down --remove-orphans --volumes
sudo docker-compose rm -f -s -v
sudo docker-compose pull
sudo docker-compose build --no-cache
sudo docker-compose up --no-recreate${additional_parameters}
