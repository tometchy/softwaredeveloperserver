using System.Text;
using server.Request;

namespace server;

public class FakeMailSender : IMailSender
{
    public async Task<MailStatus> SendAsync(string? toEmail, string? toName, string title, string body, string? from = null)
    {
        Console.WriteLine("\n\n\n FAKE SENDER -----------------------------------------------------");
        Console.WriteLine($"Sending to {toEmail} {toName}, title: {title}, body: {body}");
        Console.WriteLine("FAKE SENDER ----------------------------------------------------- \n\n\n");
        return Success.Instance;
    }

    public void InformUs(HttpContext context, DecodedRequest request)
    {
        var headersBuilder = new StringBuilder(Environment.NewLine);
        foreach (var header in context.Request.Headers)
            headersBuilder.AppendLine($"{header.Key}: {header.Value}");
        var headersDump = headersBuilder.ToString();

        var informUsMessage = $@"
        REQUEST: {request.ToString()}<br><br>

        {headersDump}<br><br>

        CLIENT IP and PORT:<br>
        {context.Connection.RemoteIpAddress}<br>
        {context.Connection.RemotePort}<br><br>

        SERVER IP and PORt:<br>
        {context.Connection.LocalIpAddress}<br>
        {context.Connection.LocalPort}<br><br>
        ";

        SendAsync("tometchy@tutanota.com", "tometchy@tutanota.com", $"{request.Email} INFORM US", informUsMessage);
    }
}