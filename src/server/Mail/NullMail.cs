namespace server.Mail;

public class NullMail : IMail
{
    public string Title => string.Empty;
    public string Content => string.Empty;

    private NullMail()
    {
    }

    public static IMail Instance { get; } = new NullMail();
}