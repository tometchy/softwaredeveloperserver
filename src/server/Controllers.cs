using Microsoft.AspNetCore.Mvc;
using server.Mail;
using server.Request;

namespace server;

public static class Materials
{
    public const string GitUntoldName = "git-untold";
    public const string GitLogName = "git-log";
    public const string GitTricksName = "git-tricks";
    public const string GitIntroductionName = "git-introduction";
    public const string KeepYourTalentsName = "keep-your-talents";
    public const string GitUntoldId = "7e0f8";
    public const string GitLogId = "1bd91";
    public const string GitTricksId = "92319";
    public const string GitIntroductionId = "ccd0b";
    public const string KeepYourTalentsId = "0c815";
    public static readonly IReadOnlyDictionary<string, string> MaterialsIds = new Dictionary<string, string>() 
    { 
        { GitUntoldName, GitUntoldId },
        { GitLogName, GitLogId },
        { GitTricksName, GitTricksId },
        { GitIntroductionName, GitIntroductionId },
        { KeepYourTalentsName, KeepYourTalentsId }
    };
    public static readonly IReadOnlyDictionary<string, string> MaterialsFileNames = new Dictionary<string, string>() 
    { 
        { GitUntoldId, "Git-Untold-Interface-Cheat-Sheet.pdf" },
        { GitLogId, "Advance-Git-Log.pdf" },
        { GitTricksId, "11-Git-Tricks-To-Boost-Your-Productivity.pdf" },
        { GitIntroductionId, "Git-Introduction-Cheat-Sheet.pdf" },
        { KeepYourTalentsId, "Employee-Turnover-Report.pdf" } 
    };
}

public static class Controllers
{
    public static WebApplication? UseControllers(this WebApplication? app, WebApplicationBuilder? builder, StatsSaver statsSaver)
    {
        app.MapGet("/" + Materials.GitUntoldId, async (HttpContext context) =>
        {
            return await ReturnMaterialFileAsync(context, context.Request.Path.ToString().Trim('/'));
        });

        app.MapGet("/" + Materials.GitLogId, async (HttpContext context) =>
        {
            return await ReturnMaterialFileAsync(context, context.Request.Path.ToString().Trim('/'));
        });

        app.MapGet("/" + Materials.GitTricksId, async (HttpContext context) =>
        {
            return await ReturnMaterialFileAsync(context, context.Request.Path.ToString().Trim('/'));
        });

        app.MapGet("/" + Materials.GitIntroductionId, async (HttpContext context) =>
        {
            return await ReturnMaterialFileAsync(context, context.Request.Path.ToString().Trim('/'));
        });

        app.MapGet("/" + Materials.KeepYourTalentsId, async (HttpContext context) =>
        {
            return await ReturnMaterialFileAsync(context, context.Request.Path.ToString().Trim('/'));
        });

        async Task<IResult> ReturnMaterialFileAsync(HttpContext context, string materialId)
        {
            var fileName = Materials.MaterialsFileNames[materialId];

            var file = await File.ReadAllBytesAsync(Path.Combine(builder.Environment.ContentRootPath, "wwwroot", "materials", fileName));
            
            statsSaver.OnMaterialDownloaded(context, materialId);

            return Results.File(file, "application/pdf", fileName);
        }

        app.MapPost("/send", async ([FromBody] Base64EncodedRequest encodedRequest, HttpContext context, [FromServices] IMailSender mailSender, [FromServices] MailCreator mailCreator) =>
        {
            Console.WriteLine("Request received, informing us");
            DecodedRequest request = new DecodedRequest(encodedRequest);
            mailSender.InformUs(context, request);
            MailerLite.SaveSender(request, context.Request.Cookies[CommonNames.QueryId] ?? string.Empty);

            var mail = NullMail.Instance;

            if (string.Equals(request.RequestType, "contact", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Contact request received");
                mail = mailCreator.CreateContactRequestConfirmationMail(request);
                statsSaver.OnContactRequested(context, request);
            }

            else if (string.Equals(request.RequestType, "material", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Materials request received");
                statsSaver.OnMaterialRequested(context, request);

                if (string.Equals(request.MaterialName, Materials.GitUntoldName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Materials request for git untold received");
                    mail = mailCreator.CreateUntoldCheatsheetMail(request);
                }

                else if (string.Equals(request.MaterialName, Materials.GitLogName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Materials request for git log received");
                    mail = mailCreator.CreateLogCheatsheetMail(request);
                }

                else if (string.Equals(request.MaterialName, Materials.GitTricksName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Materials request for git tricks received");
                    mail = mailCreator.CreateTricksMail(request);
                }

                else if (string.Equals(request.MaterialName, Materials.GitIntroductionName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Materials request for git introduction received");
                    mail = mailCreator.CreateIntroductionCheatsheetMail(request);
                }

                else if (string.Equals(request.MaterialName, Materials.KeepYourTalentsName, StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine("Materials request for keep your talents received");

                    var title = "Download Employee Turnover Report";
                    var message = $@"<div>
            Hi {request.Flname},
                <div><br>
                    here you can <a href=""https://devops.broker/{Materials.KeepYourTalentsId}"">download <i>Employee Turnover Report</i></a>.
                </div>
                <div><br>
                    Don't forget to let me know what were your impressions of the report, was there anything that surprised you?<br>
                    For me it was surprising, for example, that only 15% of the workers (worldwide) classify as engaged employees.
                    <br>Just reply to this mail and tell me :)
                </div>
                <div>
                    <br>Also, check out our other <a href=""https://devops.broker/free-materials?i=m"">downloadable free materials</a>, which you can start using right away.
                    <br>And if you find it valuable, share with others in your organization.
                </div>
                    {FooterDiv.Create(request.Email)}
            </div>";

                    mail = new RegularMail(title, message);
                }
            }

            else if (string.Equals(request.RequestType, "material-modal", StringComparison.InvariantCultureIgnoreCase))
            {
                statsSaver.OnMaterialModalRequested(context, request);
                Console.WriteLine("Material modal request received");
            }

            if(string.IsNullOrWhiteSpace(mail.Title) || string.IsNullOrWhiteSpace(mail.Content))
                return Results.Ok(Success.Instance);

            var result = await mailSender.SendAsync(request.Email, request.Flname, mail.Title, MailFactory.CreateBody(mail.Content));

            if (result is Success)
                return Results.Ok(result);

            return Results.BadRequest(result);
        });
        return app;
    }
}