using server.Request;

namespace server;

public interface IMailSender
{
    void InformUs(HttpContext context, DecodedRequest request);
    Task<MailStatus> SendAsync(string v1, string v2, string title, string message, string? from = null);
}