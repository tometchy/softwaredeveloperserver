namespace server.Events;

public class MaterialDownloaded : HttpRequestReceived
{
    private readonly string _materialId;
    private readonly string _materialName;

    public MaterialDownloaded(HttpContext context, string materialId)
        : base(context)
    {
        _materialId = materialId;
        _materialName = Materials.MaterialsIds.Single(m => string.Equals(m.Value, materialId, StringComparison.InvariantCultureIgnoreCase)).Key;
    }

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(MaterialDownloaded)}_{_materialName}.md"), ToString());
    }


    public override string ToString() => $"[{nameof(MaterialDownloaded)} >> {base.ToString()}; Material id: {_materialId}; Material name: {_materialName}]";
}