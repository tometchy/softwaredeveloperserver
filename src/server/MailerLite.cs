using System.Net.Http.Headers;
using server.Request;
using static System.Environment;
using static System.Text.Encoding;

namespace server;

public static class MailerLite
{
    private static HttpClient _client = new();

    static MailerLite()
    {
        _client.DefaultRequestHeaders.Accept.Clear();
        _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
            GetEnvironmentVariable("MAILERLITE_KEY") ?? throw new Exception("MAILERLITE_KEY not declared"));
    }

    public static void SaveSender(DecodedRequest request, string queryId)
    {
        _client.PostAsync("https://connect.mailerlite.com/api/subscribers", new StringContent($@"
{{
    ""email"": ""{request.Email}"",
    ""fields"": {{
      ""name"": ""{request.Flname}"",
      ""query"": ""{queryId}"",
      ""company"": ""{request.Company}"",
      ""phone"": ""{request.Phone}""
    }},
    ""groups"": [
        ""94790975500584869""
    ]
}}", UTF8, "application/json")).ContinueWith(r => Console.WriteLine($"Subscriber request sent, result: {r.Result}"));
    }
}