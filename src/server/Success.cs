






namespace server;

public class Success : MailStatus {
    public static MailStatus Instance { get; } = new Success();
    private Success() {}
}