namespace server.Events;

public class WebsiteVisited : HttpRequestReceived
{
    public WebsiteVisited(HttpContext context, string clientId)
        : base(context, clientId)
    {
    }

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(WebsiteVisited)}_{RequestPathParsed}.md"), ToString());
    }

    public override string ToString() => $"[{nameof(WebsiteVisited)} >> {base.ToString()}]";
}