using server.Request;

namespace server.Events;

public class MaterialRequested : HttpRequestReceived
{
    private readonly DecodedRequest _request;

    public MaterialRequested(HttpContext context, DecodedRequest request)
        : base(context) => _request = request;

    protected override void PersistEventToHdd(Context context)
    {
        base.PersistEventToHdd(context);
        File.WriteAllText(Path.Combine(context.ActionsDirectory, $"{Timestamp:yyMMdd-HHmmss}_{nameof(MaterialRequested)}_{_request.MaterialName}.md"), ToString());
    }

    public override string ToString() => $"[{nameof(MaterialRequested)} >> {base.ToString()}; {_request.ToString()}]";
}