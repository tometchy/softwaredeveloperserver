namespace server.Mail;

public interface IMail
{
    string Title { get; }
    string Content { get; }
}