using System.Collections.Concurrent;
using server.Events;
using server.Request;

namespace server;

public class StatsSaver
{
    public StatsSaver(ILogger<StatsSaver> logger) => _logger = logger;

    private readonly BlockingCollection<Event> _waitingEvents = new BlockingCollection<Event>();
    private ILogger<StatsSaver> _logger;

    public void OnWebsiteVisited(HttpContext context, string clientId) => Enqueue(new WebsiteVisited(context, clientId));
    public void OnEmailOpened(HttpContext context, string clientId, string emailId) => Enqueue(new EmailOpened(context, clientId, emailId));
    internal void OnContactRequested(HttpContext context, DecodedRequest request) => Enqueue(new ContactRequested(context, request));
    internal void OnMaterialRequested(HttpContext context, DecodedRequest request) => Enqueue(new MaterialRequested(context, request));
    internal void OnMaterialDownloaded(HttpContext context, string materialId) => Enqueue(new MaterialDownloaded(context, materialId));
    internal void OnMaterialModalRequested(HttpContext context, DecodedRequest request) => Enqueue(new MaterialModalOpened(context, request));
    

    private void Enqueue(Event e)
    {
        _logger.LogInformation($"Enqueue event: {e}");
        _waitingEvents.Add(e);
    }

    public void Start()
    {
        Task.Factory.StartNew(() => 
        {
            foreach (var e in _waitingEvents.GetConsumingEnumerable())
                Save(e);
        }, TaskCreationOptions.LongRunning);
    }

    private void Save(Event e)
    {
        try
        {
            _logger.LogInformation($"Saving event: {e}");
            e.SaveToHdd();
            var result = LinuxShell.RunInBash($"git add --all && git commit -m \"{e.GetType().Name} {e.ClientIdShort} $(date +'%F %T')\" && git fetch && git rebase && git push");
            _logger.LogInformation("Saving result: " + result);
            _logger.LogInformation($"Event saved: {e}");
        }
        catch(Exception exception)
        {
            _logger.LogError(exception, "Exception while saving event");
        }
    }
}