if [ -d "/dist" ]; then
    echo "Publishing to mounted /dist directory" 
else
    echo "/dist directory not mounted to host; Mount it like this: -v /app/src/server/wwwroot:/dist"
    exit 1
fi

while true
do
    echo "Fetching website sources"
    git fetch
    git reset --hard origin/master
    if [ "$DEBUG" == "true" ]; then
        echo "Debug environment detected, replacing url to localhost"
        sed -i 's/https:\/\/devops.broker/https:\/\/localhost/g' _config.yml
    fi

    last_commit_id="$(git log --pretty=format:%H -1)"
    echo "Last commit id: ${last_commit_id} - comparing with previous one"

    if [ "${last_commit_id}" != "$(cat _last_commit_id)" ]; then
        echo "Regenerating, website has been changed since version: $(cat _last_commit_id)"
        echo "${last_commit_id}" > _last_commit_id
        bundle exec jekyll build --destination /dist

        # tbd build to temporary directory
        # tbd minify temp dir
        # create file with path and hash of every file
        # compare with previous version, put every file missing in new version to todelete file
        # compare with previous version, remove from temp dir files which didn't change
        # move new and changed files to /dist
        # remove from /dist files marked to delete
    else
        echo "Website didn't change, skipping regeneration"
    fi

    sleep 30s
done