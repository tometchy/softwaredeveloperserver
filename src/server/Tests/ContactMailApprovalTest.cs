using NUnit.Framework;
using server.Mail;

namespace server.Tests;

public class ContactMailApprovalTest
{
    private const string FlName = "Alice";

    [Test]
    public void Test()
    {
        var response = new MailCreator().CreateContactRequestConfirmationMail(new FakeRequest { Flname = FlName });
        Assert.AreEqual("I have received your message", response.Title);
        Assert.AreEqual(@"
<div>
        Hi Alice,
            <div><br>
                I have just received your message and will reply to you as soon as possible.
            </div>
            <div><br>
                In the meanwhile check out our <a href=""https://devops.broker/free-materials?i=m"">downloadable free materials</a>, which you can start using right away.
                <br>If you find them valuable, share them with others in your organization.
            </div>
            <div><br>
                After all, <b>you make a strong team together</b> 💪
                <br><br><i>Give my regards to the team,<br>
                Tom Skraskowski<br><a href=""https://www.devops.broker"">DevOps.Broker</a></i><br>
            </div>
        </div>
", response.Content);
    }
}