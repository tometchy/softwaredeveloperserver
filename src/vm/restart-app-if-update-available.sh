#!/bin/bash

user=$1

if [ -z "$user" ]
then
    echo "You must pass user name of repo owner"
    exit 1
fi

app_version_directory="/home/$user/app-version"

the_newest=$(cat $app_version_directory/the-newest)
echo "The newest: ${the_newest}"

if [ "${the_newest}" != "$(cat $app_version_directory/current)" ]; then
    echo "Restarting, app has been changed since version: $(cat $app_version_directory/current)"

    sudo docker-compose -f /home/$user/app/src/docker-compose.yml down
    sudo docker-compose -f /home/$user/app/src/docker-compose.yml up --build -d

    echo "${the_newest}" > $app_version_directory/current
    echo "Current app version: $(cat $app_version_directory/current)"
else
    echo "App didn't change, skipping restart"
fi